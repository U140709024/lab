
public class FindMin {
	
	public static void main(String[] args){
		
		int value1=Integer.parseInt(args[0]);
		int value2=Integer.parseInt(args[1]);
		int value3=Integer.parseInt(args[2]);
        int result1,result2;

        boolean someCondition = value1 > value2;
       
        result1 = someCondition ? value2 : value1;
        
        boolean someCondition2 = value3 > result1;
        
        result2 = someCondition2 ? result1 : value3;
        
        System.out.println(result2);

	}
}
