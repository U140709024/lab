public class FindGrade {

	public static void main(String[] args) {
	
		String grade;
		int score = Integer.parseInt(args[0]);
		
		boolean condition1 = 90 <= score && score <= 100;
		boolean condition2 = 80 <= score && score < 90;
		boolean condition3 = 70 <= score && score < 80;
		boolean condition4 = 60 <= score && score < 70;
		boolean condition5 = 0 <= score && score < 60;
		
		
		if(condition1) {
			grade = "A";
			System.out.println("Your grade is " + grade);
		}
		else if(condition2) {
			grade = "B";
			System.out.println("Your grade is " + grade);
		}
		else if(condition3) {
			grade = "C";
			System.out.println("Your grade is " + grade);
		}
		else if(condition4) {
			grade = "D";
			System.out.println("Your grade is " + grade);
		}
		else if(condition5) {
			grade = "F";
			System.out.println("Your grade is " + grade);
		}
		else {
			System.out.println("It is not a valid score!");
		}
	}
}