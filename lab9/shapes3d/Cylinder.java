package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
	
	protected int height;

	public Cylinder(int radius) {
		super(radius);
		// TODO Auto-generated constructor stub
	}
	
	public double area() {
		return 2 * super.area() 
				+ 2 *Math.PI * radius;
	}
	
	public double volume() {
		return super.area() * height;
	}

	@Override
	public String toString() {
		return "Cylinder [height=" + height + ", radius=" + radius + ", area()=" + area() + ", volume()=" + volume()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
	
	

}
