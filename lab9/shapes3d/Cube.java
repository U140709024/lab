package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
	
	protected int height;
	
	public Cube(int w, int l) {
		super(w, l);
		// TODO Auto-generated constructor stub
	}

	public int area() {
		return super.area() * 6;
	}
	
	public double volume() {
		return super.area() * height;
	}
	

}
