package shapes2d;

public class Square {
	protected int length;
	protected int width;
	
	public Square(int w,int l) {
		this.width = w;
		this.length = l;
	}
	
	public int area() {
		return width * length;
	}

	
	public String toString() {
		return "Square [length=" + length + ", width=" + width + ", area()=" + area() + "]";
	}
	
	
}
