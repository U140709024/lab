
public class Main {

	
	public static void main(String[] main) {
		Rectangle myRectangle; 
		Point topleft = new Point(2,5);
		myRectangle = new Rectangle(2,5,topleft);
		
		System.out.println("Area of the rectangle is "+myRectangle.area());
		System.out.println("Perimeter of the rectangle is "+myRectangle.perimeter());
		System.out.println("Corner points of the rectangle are ");
		for(int i =0;i<4;i++)
			System.out.println(myRectangle.corners()[i].xCoord +", "+myRectangle.corners()[i].yCoord);
		
		Point center = new Point(2,2);
		Circle myCircle = new Circle(10,center);
		System.out.println("Area of the circle is "+myCircle.area());
		System.out.println("Perimeter of the circle is "+myCircle.perimeter());
		System.out.println("intersect or not :"+myCircle.intersects(new Circle(3, new Point(5, 5))));
		
	}
}
