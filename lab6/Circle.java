
public class Circle {
	int radius;
	Point center;
	
	public Circle(int radius,Point p){
		this.radius = radius;
		this.center = p;
	}

	public double area(){
		return Math.PI * Math.pow(radius, 2);
	}

	public double perimeter(){
		return 2 * Math.PI * radius;
	}
	
	
	public boolean intersects(Object obj){
		if (obj instanceof Circle){
			Circle circle = (Circle)obj;
			int D = (int) Math.sqrt((Math.pow((circle.center.xCoord - center.xCoord),2) + Math.pow((circle.center.yCoord - center.yCoord),2)));
			if ((circle.radius + radius) > D && D > (Math.abs(circle.radius - radius))){
				return true;
			}
		}
		return false;
	}	
}

