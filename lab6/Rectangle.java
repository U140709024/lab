
public class Rectangle {

	int sideA,sideB;
	Point topleft;
	
	
	public Rectangle(int x, int y,Point p){
		sideA = x;
		sideB = y;
		topleft = p;
	}
	

	public int area() {
		return sideA * sideB;
	}
	
	public int perimeter() {
		return 2 * (sideA * sideB);
	}
	
	public Point[] corners() {
		Point topright = new Point(0,0);
		Point bottomleft = new Point(0,0);
		Point bottomright = new Point(0,0);
		Point[] myCorners = {topright,topleft,bottomleft,bottomright}; 
		
		bottomleft.xCoord = topleft.xCoord;
		bottomleft.yCoord = topleft.yCoord-sideA;
		
		bottomright.xCoord = bottomleft.xCoord + sideB;
		bottomright.yCoord = bottomleft.yCoord;
		
		topright.xCoord = bottomright.xCoord;
		topright.yCoord = topleft.yCoord;
		
		return myCorners;
	}
}
