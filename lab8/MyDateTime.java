
public class MyDateTime {
	
	MyDate myDate;
	MyTime myTime;
	
	public MyDateTime() {
		
	}
	
	public MyDateTime(MyDate myDate,MyTime myTime) {
		this.myDate = myDate;
		this.myTime = myTime;
}
	
	public void incrementDay() {
		myDate.incrementDay();
		
	}
	
	public void incrementHour() {
		myTime.incrementHour();
			
	}
	public void incrementHour(int count) {
		myTime.incrementHour(count,myDate);
	}
	public void decrementHour(int count) {
		myTime.decrementHour(count,myDate);
	}
	public void incrementMinute(int count) {
		myTime.incrementMinute(count,myDate);
	}
	public void decrementMinute(int count) {
		myTime.decrementMinute(count,myDate);
	}
	public void incrementYear(int count) {
		myDate.incrementYear(count);
	}
	public void decrementDay() {
		myDate.decrementDay();
	}
	public void decrementYear() {
		myDate.decrementYear();
	}
	public void decrementMonth() {
		myDate.decrementMonth();
	}
	public void incrementDay(int count) {
		myDate.incrementDay(count);
	}
	public void decrementMonth(int count) {
		myDate.decrementMonth(count);
	}
	public void decrementDay(int count) {
		myDate.decrementDay(count);
	}
	public void incrementMonth() {
		myDate.incrementMonth();
	}
	public void incrementMonth(int count) {
		myDate.incrementMonth(count);
	}
	public void decrementYear(int count) {
		myDate.decrementYear(count);
	}
	public void incrementYear() {
		myDate.incrementYear();
	}
	public String dayTimeDifference(MyDateTime anotherDate) {
		return "";
	}
	public boolean isBefore(MyDateTime anotherDate) {
		return true;
		
		
	}
	public boolean isAfter(MyDateTime anotherDate) {
		return true;
		
	}
	public String toString(){
		return myDate.toString() +" " +myTime.toString(); 
	}



	

}
