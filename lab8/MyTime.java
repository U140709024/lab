
public class MyTime {
	int hour;
	int minute;
	
	
	
	public MyTime(int hour,int minute) {
		this.hour = hour;
		this.minute = minute;

	}
	
	
	public void incrementHour() {
		hour++;
	}
	public void incrementHour(int count,MyDate date) {
		int newHour = (hour + count) % 24;
		int dayChange = 0;
		
		if (newHour < 0) {
			newHour += 24;
			dayChange = -1;
		}
		
		dayChange += (hour + count) / 24;
		hour = newHour;
		date.day += dayChange;
		if (date.day > date.maxDays[date.month]){
			date.day = date.maxDays[date.month];
			if(date.month ==1 && date.day ==29 && !date.inLeapYear()){
				date.day =28;
			}
		}	
	}
	public void decrementHour(int count,MyDate date) {
		int newHour = (hour - count) % 24;
		int dayChange = 0;
		
		if (newHour < 0) {
			newHour += 24;
			dayChange = 1;
	
		}
		
		dayChange += (count) / 24;
		hour = newHour;
		date.decrementDay(dayChange);
		
	}
	public void incrementMinute(int count,MyDate date) {
		int newMin = (minute + count) % 60;
		int hourChange = 0;
		
		if (newMin < 0) {
			newMin += 60;
			hourChange = -1;
		}
		
		hourChange += (minute +count) / 60;
		minute = newMin;
		hour += hourChange;
		
		if (hour> 24){
			date.incrementDay(hour / 24);
			hour = hour % 24;
			
		}
	}
	public void decrementMinute(int count,MyDate date) {
		incrementMinute(-count,date);
	}
	
	public String toString() {
		if(hour<10 && minute<10)
			return "0"+ hour +":"+"0"+minute;
		if(hour<10 && minute>10)
			return "0"+ hour +":"+minute;
		return hour +":"+minute;
	}


}
