package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	ArrayList shapes = new ArrayList();
	

	public double calculateTotalArea(){
		double totalArea = 0;
		for (Object shape : shapes){
			if (shape instanceof Circle){
				Circle circle = (Circle) shape;
				totalArea += circle.area();
			}else if (shape instanceof Rectangle){
				Rectangle rect= (Rectangle) shape;
				totalArea += rect.area();
			}else if (shape instanceof Square){
				Square sq= (Square) shape;
				totalArea += sq.area();
			}
		}
		return totalArea;
	}


	public void add(Object o) {
		shapes.add(o);
		
	}
	
}
