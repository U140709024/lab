package drawing.version2;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class TestDrawing {

	public static void main(String[] args) {
		
		Drawing drawing = new Drawing();
		
		drawing.add(new Circle(4));
		drawing.add(new Rectangle(5,4));
		drawing.add(new Square(5));
		

		System.out.println("Total area = " + drawing.calculateTotalArea());
	}

}
