package drawing.version3;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;

public class Drawing {
	
	ArrayList<Shape> shapes = new ArrayList<Shape>();
	
	public void addShape(Shape shape){
		shapes.add(shape);
	}
	
	public double calculateTotalArea(){
		
		double totalArea = 0;
		for (Shape shape : shapes){
			totalArea += shape.area();
		}
		return totalArea;
	}
}
