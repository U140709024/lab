package drawing.version3;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;

public class TestDrawing {

	public static void main(String[] args) {
		
		Drawing drawing = new Drawing();
		Shape shape = new Circle(4);
		Shape shape2 = new Square(5);
		Shape shape3 = new Rectangle(5,4);
		
		drawing.addShape(shape);
		drawing.addShape(shape2);
		drawing.addShape(shape3);		

		System.out.println("Total area = " + drawing.calculateTotalArea());
	}

}
