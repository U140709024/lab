import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);
		
		int count=0;

		while(count<9) {
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();
			
			if(row > 3 || col > 3 || board[row - 1][col - 1] != ' ' ) {
				boolean requireAnother = true;
				while(requireAnother) {
					if(row > 3 || col > 3)
						System.out.println("It is out of range!");
					else if(board[row - 1][col - 1] != ' ')
						System.out.println("It is occupied!");
					System.out.print("Player 1 enter another row number:");
					row = reader.nextInt();
					System.out.print("Player 1 enter another column number:");
					col = reader.nextInt();
					if(row > 3 || col > 3  ||  board[row - 1][col - 1] != ' ' )
						requireAnother =true;
					else
						requireAnother =false;
				}
				board[row - 1][col - 1] = 'X';
				printBoard(board);
				
			}else if(board[row - 1][col - 1] == ' ') {
				board[row - 1][col - 1] = 'X';
				printBoard(board);
			}
			if(checkBoard(board)) {
				System.out.println("Game has finished!");
				System.out.println("Player1 won!");
				break;
			}
			count++;
			if(count<9) {
				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				col = reader.nextInt();
				//boolean isValid = true;
				
				if(row > 3 || col > 3 || board[row - 1][col - 1] != ' ' ) {
					boolean requireAnother = true;
					while(requireAnother) {
						if(row > 3 || col > 3)
							System.out.println("It is out of range!");
						else if(board[row - 1][col - 1] != ' ')
							System.out.println("It is occupied!");
						System.out.print("Player 2 enter another row number:");
						row = reader.nextInt();
						System.out.print("Player 2 enter another column number:");
						col = reader.nextInt();
						if(row > 3 || col > 3  ||  board[row - 1][col - 1] != ' ' )
							requireAnother =true;
						else
							requireAnother =false;
					}
				
					board[row - 1][col - 1] = 'O';
					printBoard(board);
					
				}else if(board[row - 1][col - 1] == ' ') {
					
					board[row - 1][col - 1] = 'O';
					printBoard(board);
				} 
			}
			if(checkBoard(board)) {
				System.out.println("Game has finished!");
				System.out.println("Player2 won!");
				break;
			}
			count++;
		}
		
		if(count>=9) {
			System.out.println("Game has finished!");
			System.out.println("Game ended with a draw!");
		}

		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
	
	public static boolean checkBoard(char[][] board) {
		
		for (int row = 0; row < 3; ++row) {
		
				if(board[row][0] == 'X' && board[row][1] == 'X' &&  board[row][2] == 'X') {
					return true;	
				}
				if(board[row][0] == 'O' && board[row][1] == 'O' &&  board[row][2] == 'O') {
					return true;	
				}
				if(board[0][row] == 'X' && board[1][row] =='X' && board[2][row] =='X' ) {
					return true;
				}
				if(board[0][row] == 'O' && board[1][row] =='O' && board[2][row] =='O' ) {
					return true;
				}
		}
		if( board[0][0] == 'X' && board[1][1] =='X' && board[2][2] =='X') {
			return true;
		}
		if( board[0][0] == 'O' && board[1][1] =='O' && board[2][2] =='O') {
			return true;
		}
		if(board[0][2] == 'X' && board[1][1] =='X' && board[2][0] =='X') {
			return true;
		}
		if(board[0][2] == 'O' && board[1][1] =='O' && board[2][0] =='O') {
			return true;
		}
		return false;				
		
	}

}
