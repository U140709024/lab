
public class Rectangle {

	private double height;
	private double lenght;
	
	
	public Rectangle(double h, double l) {
		super();
		this.height = h;
		this.lenght = l;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public double getLenght() {
		return lenght;
	}
	public void setLenght(int lenght) {
		this.lenght = lenght;
	}
	
	public double area() {
		return height * lenght;
	}
	
	public double perimeter() {
		return 2 * (height + lenght);
	}
	
}
