
public class Midterm {
	
	public static void main(String args[]){
		
		//Question1
		System.out.println(triType(4,4,4) + " "+triType(4,4,3)+ " " +
		triType(5,4,3) + " " + triType(2,3,4) + " " + triType(4,2,1));
		
		//Output of Question 1 -> 1 3 2 4 0
		
		//Qustion2
		System.out.println(max(22,15,4,7));
		
		//Qustion5
		System.out.println(divide(27,5));
		int[] myArr = {200,5,7,9,100,49,52,78,4,1,3,5,86,99};
		int[] myArr2 = {7,7,7,7};
		
		//Qustion3
		System.out.println(secondMax(myArr));
	}
	
	//Question1
	public static int triType (int a, int b, int c){
		if (a > b) { a = a + b; b = a - b; a = a - b;}
		if (b > c) { b = b + c; c = b - c; b = b - c; }
		if (a + b <= c) return 0;
		else if ((a == b) && (b == c)) return 1;
		else if (a*a + b*b == c*c) return 2;
		else if ((a == b) || (b == c) || (a == c)) return 3;
		return 4;
	}
	
	//Qustion2
	private static int max(int a, int b, int c, int d) {
		int max = a;
		if(b>a)
			max =b;
		if(c>max)
			max=c;
		if(d>max)
			max=d;
		return max;
	}
	//Qustion3
	public static int secondMax(int[] arr) {
		int max=-1;
		int secondMax=-1;
		
		for(int i=0;i<arr.length;i++) {
			if(arr[i] > max) {
				secondMax = max;
				max=arr[i];
			}
			if(arr[i]>secondMax && arr[i] < max)
				secondMax = arr[i];
		}
		//System.out.println(max);
		return secondMax;
		
	}
	//Qustion4
	public static int function(int n) {
			if(n == 0)
				return 1;
			else
				return 1 + (1 /function(n-1));
			
	}
	
	//Qustion5
	private static int divide(int divident,int divisor) {
		if(divident < divisor)
			return 0;
		else
			return 1 + divide(divident-divisor,divisor);
		
	}
	
	

}
