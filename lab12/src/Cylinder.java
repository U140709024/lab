//Question10

public class Cylinder extends Circle {
	

	protected double height;
	
	public Cylinder(int radius,double height) {
		super(radius);
		this.height = height;
		
	}
	
	public double area() {
		
		return 2 * super.area() + super.perimeter() * height;
	}

	public String toString() {
		return super.toString() + "heiht = "+height;
	}
	
	public double volume() {
		return super.area() * height;
	}


}
