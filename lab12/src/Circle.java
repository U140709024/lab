
public class Circle {

	protected int radius;
	static double PI = 3.14;
	
	public Circle(int radius) {
		super();
		this.radius = radius;
	}
	
	public double area() {
		return radius * radius * Circle.PI;
	}
	
	public double perimeter() {
		return 2 * radius * Circle.PI;
	}

	public String toString() {
		return "Circle [radius=" + radius + ", area()=" + area() + "]";
	}
	
	
}
