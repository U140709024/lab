
public class GCDRec {
	
	public static void main(String[] args) {
		
		System.out.println(GCD(1071,462));
	}
	
	public static int GCD(int a,int b) {
		
		int r=1;
		if(a > b)
		{
			int q =  a / b;
			r = a % b;
			if(r != 0) 
			{
				//System.out.println("a = "+a+" ,b = "+b);
				//System.out.println("b = "+b+" ,r = "+r);
				 return GCD(b,r);
			}
		}
		return b;

	}

}
