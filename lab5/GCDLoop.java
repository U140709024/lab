
public class GCDLoop {
	
	public static void main(String[] args) {
		
		System.out.println(GCD(1500,100));
	}
	
	public static int GCD(int a,int b) {
		
		if(a > b)
		{
			int r = 1;
			while(r !=0 ) 
			{
				int q =  a / b;
				r = a % b;
				a = b;
				if(r !=0)
					b = r ;
				
			}
		}
		
		return b;
	}

}
