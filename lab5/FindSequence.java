import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

	public static void main(String[] args) throws FileNotFoundException{
		int matrix[][] = readMatrix();
		printMatrix(matrix);

		boolean found = false;
search: for (int i=0; i< matrix.length; i++) {
			for (int j=0; j < matrix[i].length; j++) {
				if (search(0,matrix, i,j)){
					found = true;
					break search;
				}
			}
		}
			
		if (found) {
			System.out.println("A sequence is found");
		}
		printMatrix(matrix);

	}

	private static boolean search (int number, int[][]matrix, int row, int col) {
		
		int up=0,right=0,bottom=0,left=0;
		
		if(matrix[row][col] == number && number <10)
		{
			
			number = number + 1;
			if(col !=0)
				left = matrix[row][col-1];
			
			if(row != 0) 
				up = matrix[row-1][col];				
			
			if(col != matrix.length-1)
				right = matrix[row][col+1];
			
			if (row != matrix.length-1) 
				bottom = matrix[row+1][col];
			
			if(left == number) {
				matrix[row][col] = Math.abs(number-10);
				//System.out.println("NUMBER : "+(number-1) +"  ,up : "+up+" ,right : "+right+" ,bottom : "+bottom+" ,left : "+left+" ,col : "+col+" ,row : "+row);
				return search (left,matrix,row,col-1);
			}
			if(up == number) {
				matrix[row][col] = Math.abs(number-10);
				//System.out.println("NUMBER : "+(number-1) +"  ,up : "+up+" ,right : "+right+" ,bottom : "+bottom+" ,left : "+left+" ,col : "+col+" ,row : "+row);
				return search (up,matrix,row-1,col);
			}
			if(right == number) {
				matrix[row][col] = Math.abs(number-10);
				//System.out.println("NUMBER : "+(number-1)+"  ,up : "+up+" ,right : "+right+" ,bottom : "+bottom+" ,left : "+left+" ,col : "+col+" ,row : "+row);
				return search (right,matrix,row,col+1);
			}
			if(bottom == number) {
				matrix[row][col] = Math.abs(number-10);
				//System.out.println("NUMBER : "+(number-1) +"  ,up : "+up+" ,right : "+right+" ,bottom : "+bottom+" ,left : "+left+" ,col : "+col+" ,row : "+row);
				return search (bottom,matrix,row+1,col);
			}
			
			//System.out.println("NUMBER : "+(number-1) +"  ,up : "+up+" ,right : "+right+" ,bottom : "+bottom+" ,left : "+left+" ,col : "+col+" ,row : "+row);
			matrix[row][col] = Math.abs(number-10);
			if(number == 10) {
				return true;
			}			
			
		}
		return false;

	}

	private static int[][] readMatrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("matrix.txt"); // if you run from command line use  new File("matrix.txt") instead

		try (Scanner sc = new Scanner(file)){

			
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
