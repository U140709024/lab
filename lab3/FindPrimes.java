import java.util.Scanner;

public class FindPrimes {
	
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		int number = reader.nextInt();
		boolean isPrime = true;
		
		for(int i = 2 ; i <= number; i++) {
			for(int j = 2 ; j <=Math.sqrt(i); j++) {
				if(i % j == 0) {
					isPrime=false;
					break;
				}
			}	
			if(isPrime) {
				System.out.print(i+", ");
			}
			isPrime = true;
		}
	}	
}
